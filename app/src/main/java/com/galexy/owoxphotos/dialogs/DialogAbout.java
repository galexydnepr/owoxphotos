package com.galexy.owoxphotos.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.galexy.owoxphotos.BuildConfig;
import com.galexy.owoxphotos.R;

public class DialogAbout {

    Activity activity;
    AlertDialog.Builder dialogBuilder;

    public DialogAbout(Activity activity) {

        this.activity = activity;
        onCreateDialog();
    }

    public void openDialog() {
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    protected void onCreateDialog() {

        dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater factory = LayoutInflater.from(activity);
        final View view = factory.inflate(R.layout.dialog_about, null);
        dialogBuilder.setView(view);

        PackageManager pm = activity.getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = pm.getApplicationInfo(activity.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        String appLabel = "";
        if (applicationInfo != null){
            appLabel = (String) pm.getApplicationLabel(applicationInfo);
        }
        String title = appLabel + " v." + BuildConfig.VERSION_NAME;
        TextView titleView=(TextView)view.findViewById(R.id.dialog_about_title);
        titleView.setText(title);
        dialogBuilder.setPositiveButton(R.string.text_ok, buttonClickListener);
    }


   private DialogInterface.OnClickListener buttonClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    break;
            }
        }
    };
}
