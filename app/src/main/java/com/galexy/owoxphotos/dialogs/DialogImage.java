package com.galexy.owoxphotos.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.galexy.owoxphotos.R;
import com.galexy.owoxphotos.models.Photo;
import com.squareup.picasso.Picasso;

public class DialogImage {

    private Activity mActivity;
    private AlertDialog.Builder dialogBuilder;
    private Photo mPhoto;
    private ImageDialogListener mListener;

    public DialogImage(Activity activity, Photo photo, ImageDialogListener listener) {
        mPhoto = photo;
        mActivity = activity;
        mListener = listener;
        onCreateDialog();
    }

    public void openDialog() {
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    protected void onCreateDialog() {
        dialogBuilder = new AlertDialog.Builder(mActivity);

        LayoutInflater factory = LayoutInflater.from(mActivity);
        final View view = factory.inflate(R.layout.dialog_image, null);
        dialogBuilder.setView(view);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageDialogImage);
        String path = mActivity.getResources().getBoolean(R.bool.isTablet) ? mPhoto.getUrls().getFull() :mPhoto.getUrls().getRegular();

        Picasso.with(factory.getContext())
                .load(path)
                .into(imageView);


        dialogBuilder.setPositiveButton(R.string.text_share, buttonClickListener);
        dialogBuilder.setNegativeButton(R.string.text_download, buttonClickListener);
    }


   private DialogInterface.OnClickListener buttonClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    mListener.onDialogImageShareClick(mPhoto);
                    break;

                case Dialog.BUTTON_NEGATIVE:
                    mListener.onDialogImageDownloadClick(mPhoto);
                    break;
            }
        }
    };

    public interface ImageDialogListener {
        void onDialogImageShareClick(Photo selectedPhoto);

        void onDialogImageDownloadClick(Photo selectedPhoto);
    }

}
