package com.galexy.owoxphotos.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {

    /*{
       id: "A7RzCegedb4",
created_at: "2017-10-21T07:03:02-04:00",
updated_at: "2017-10-22T08:15:15-04:00",
width: 3928,
height: 4910,
color: "#EB9BA8",
likes: 99,
liked_by_user: false,
description: null,
user: {},
current_user_collections: [ ],
urls: {
raw: "https://images.unsplash.com/photo-1508583732154-e9ff899f8534",
full: "https://images.unsplash.com/photo-1508583732154-e9ff899f8534?ixlib=rb-0.3.5&q=85&fm=jpg&crop=entropy&cs=srgb&s=701ceadb578422b7fe3709a933767139",
regular: "https://images.unsplash.com/photo-1508583732154-e9ff899f8534?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&s=25ab7065babbba595ee4cdc6065adfbb",
small: "https://images.unsplash.com/photo-1508583732154-e9ff899f8534?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&s=6d13bb6cf546902f1f6f75a2b878b88e",
thumb: "https://images.unsplash.com/photo-1508583732154-e9ff899f8534?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=848e47d561a4f51b6a68c2a30394312e"
},
categories: [ ],
links: {
self: "https://api.unsplash.com/photos/A7RzCegedb4",
html: "https://unsplash.com/photos/A7RzCegedb4",
download: "https://unsplash.com/photos/A7RzCegedb4/download",
download_location: "https://api.unsplash.com/photos/A7RzCegedb4/download"
}
    }*/
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("liked_by_user")
    @Expose
    private boolean likedByUser;
    @SerializedName("description")
    @Expose
    private String  description;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("current_user_collections")
    @Expose
    private String[] currentUserCollections ;
    @SerializedName("urls")
    @Expose
    private Urls urls;
    @SerializedName("categories")
    @Expose
    private String[] categories ;
    @SerializedName("links")
    @Expose
    private Links links;


    public Photo() {
    }

    public Photo(@NonNull String id, String createdAt, String updatedAt, int width, int height, String color, int likes,
                 boolean likedByUser, String  description, User user, String[] currentUserCollections, Urls urls,
                 String[] categories, Links links) {
        super();
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.width = width;
        this.height = height;
        this.color = color;
        this.likes = likes;
        this.likedByUser = likedByUser;
        this.description = description;
        this.user = user;
        this.currentUserCollections = currentUserCollections;
        this.urls = urls;
        this.categories = categories;
        this.links = links;
    }
    @NonNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? "" : id;
    }

       public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public String  getDescription() {
        return description;
    }

    public void setDescription(String  description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String[] getCurrentUserCollections() {
        return currentUserCollections;
    }

    public void setCurrentUserCollections(String[] currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    public Urls getUrls() {
        return urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}