package com.galexy.owoxphotos.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Search  {

    /*{
     {
total: 3163,
total_pages: 317,
results: [
    }*/
    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("total_pages")
    @Expose
    private int total_pages;
    @SerializedName("links")
    @Expose
    private Photo[] photos;

    public Search() {
    }

    public Search(int total, int total_pages, Photo[] photos) {
        super();

        this.total = total;
        this.total_pages = total_pages;
        this.photos = photos;
    }


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public Photo[] getPhotos() {
        return photos;
    }

    public void setPhotos(Photo[] photos) {
        this.photos = photos;
    }

}