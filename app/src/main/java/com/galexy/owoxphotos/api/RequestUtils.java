package com.galexy.owoxphotos.api;

import android.support.annotation.NonNull;

import com.galexy.owoxphotos.api.interfaces.RequestJsonInterface;
import com.galexy.owoxphotos.api.interfaces.RequestStringInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RequestUtils {
    static OkHttpClient clientOkHttp = new OkHttpClient();

    public static void baseJsonRequestWithBody(@NonNull String requestUrl, final RequestJsonInterface requestJsonInterface) {
        clientOkHttp.newCall(new Request.Builder().url(requestUrl)
                .build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                requestJsonInterface.onError(e.getLocalizedMessage());
            }

            public void onResponse(Call call, Response responseIn) throws IOException {
                Response response = responseIn;
                if (!response.isSuccessful()) {
                    requestJsonInterface.onError("");
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                        requestJsonInterface.onCompleteJson(jsonObject);
                } catch (JSONException e) {
                    requestJsonInterface.onError("");
                }
            }
        });
    }

    public static void baseStringRequest(@NonNull String requestUrlPart, final RequestStringInterface requestStringInterface) {
        clientOkHttp.newCall(new Request.Builder().url(requestUrlPart)
                .build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                requestStringInterface.onError(e.getLocalizedMessage());
            }

            public void onResponse(Call call, Response responseIn) throws IOException {
                Response response = responseIn;
                if (!response.isSuccessful()) {
                    requestStringInterface.onError("");
                }else{
                    requestStringInterface.onComplete(response.body().string());
                }
            }
        });
    }
}
