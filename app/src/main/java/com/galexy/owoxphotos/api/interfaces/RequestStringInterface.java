package com.galexy.owoxphotos.api.interfaces;

public interface RequestStringInterface {

    void onComplete(String result);

    void onError(String str);

}
