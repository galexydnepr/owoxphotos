package com.galexy.owoxphotos.api.interfaces;

import org.json.JSONObject;


public interface RequestJsonInterface {

    void onCompleteJson(JSONObject jSONObject);

    void onError(String str);

}
