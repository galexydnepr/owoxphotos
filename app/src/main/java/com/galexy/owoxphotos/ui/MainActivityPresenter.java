package com.galexy.owoxphotos.ui;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.galexy.owoxphotos.BuildConfig;
import com.galexy.owoxphotos.api.interfaces.RequestJsonInterface;
import com.galexy.owoxphotos.api.interfaces.RequestStringInterface;
import com.galexy.owoxphotos.api.RequestUtils;
import com.galexy.owoxphotos.models.Photo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivityPresenter implements MainActivityBus.Presenter, RequestStringInterface, RequestJsonInterface {

    @NonNull
    private MainActivity callActivity;
    @NonNull
    private MainActivityBus.SystemLink systemLink;

    private int currentPhotosPage = 1;

    private String wordToSearch;

    MainActivityPresenter(@NonNull MainActivityBus.SystemLink systemLink, @NonNull MainActivity loadingActivity) {
        this.systemLink = systemLink;
        this.callActivity = loadingActivity;
        callServer(1);
    }

    @Override
    public void refreshItems() {
        currentPhotosPage = 1;
        if (!TextUtils.isEmpty(wordToSearch)) {
            callSearchServer(currentPhotosPage, wordToSearch);
        } else {
            callServer(currentPhotosPage);
        }
    }

    @Override
    public void performSearch(String word) {
        systemLink.clearList();
        wordToSearch = word;
        currentPhotosPage = 1;
        callSearchServer(currentPhotosPage, wordToSearch);
    }

    @Override
    public void getMoreFromServer() {
        currentPhotosPage++;
        if (!TextUtils.isEmpty(wordToSearch)) {
            callSearchServer(currentPhotosPage, wordToSearch);
        } else {
            callServer(currentPhotosPage);
        }
    }

    @Override
    public void cancelSearch() {
        wordToSearch="";
        systemLink.clearList();
        refreshItems();
    }

    @Override
    public void onComplete(String result) {
        try {
            if (result.contains("errors")) {
                JSONObject errorObject = new JSONObject(result);
                JSONArray errorArray = errorObject.optJSONArray("errors");
                systemLink.showError(errorObject.optString(errorArray.get(0).toString()));
            } else {
                parseIncommingData(result);
            }
        } catch (JSONException e) {
            systemLink.showError(e.getLocalizedMessage());

        }
    }

    private void parseIncommingData(String result) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Photo>>() {
        }.getType();
        final ArrayList<Photo> posts = (ArrayList<Photo>) gson.fromJson(result, listType);
        callActivity.runOnUiThread(new Runnable() {
            public void run() {
                systemLink.updatePhotosList(posts);
            }
        });
    }


    @Override
    public void onCompleteJson(JSONObject jSONObject) {
        String error = jSONObject.optString("errors", "");
        if (TextUtils.isEmpty(error)) {
            String result = jSONObject.optString("results");
            Gson gson = new Gson();
            Type listType = new TypeToken<List<Photo>>() {
            }.getType();
            final ArrayList<Photo> posts = (ArrayList<Photo>) gson.fromJson(result, listType);
            callActivity.runOnUiThread(new Runnable() {
                public void run() {
                    systemLink.updatePhotosList(posts);
                }
            });
        }

    }

    @Override
    public void onError(String str) {
        systemLink.showError(str);
    }


    private void callServer(int currentPhotosPage) {
        String url = BuildConfig.API_ENDPOINT + "photos?client_id=" + BuildConfig.CLIENT_ID + "&per_page=20&page=" + currentPhotosPage;
        RequestUtils.baseStringRequest(url, this);
    }

    private void callSearchServer(int currentPhotosPage, String wordToSearch) {
        String url = BuildConfig.API_ENDPOINT + "search/photos?client_id=" + BuildConfig.CLIENT_ID + "&per_page=10&page=" + currentPhotosPage + "&query=" + wordToSearch;
        RequestUtils.baseJsonRequestWithBody(url, this);
    }
}
