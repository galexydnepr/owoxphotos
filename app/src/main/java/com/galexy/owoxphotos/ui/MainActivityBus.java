package com.galexy.owoxphotos.ui;

import com.galexy.owoxphotos.models.Photo;

import java.util.ArrayList;

public class MainActivityBus {
    public interface SystemLink {
        void goToInventory();

        void updatePhotosList(ArrayList<Photo> posts);

        void showError(String errorText);

        void clearList();
    }

    public interface Presenter {

        void refreshItems();

        void performSearch(String wordToSearch);

        void getMoreFromServer();

        void cancelSearch();
    }
}