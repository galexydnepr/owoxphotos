package com.galexy.owoxphotos.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.galexy.owoxphotos.BuildConfig;
import com.galexy.owoxphotos.R;
import com.galexy.owoxphotos.adapters.PhotosListAdapter;
import com.galexy.owoxphotos.dialogs.DialogImage;
import com.galexy.owoxphotos.models.Photo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainActivityBus.SystemLink, DialogImage.ImageDialogListener {

    @BindView(R.id.gridView)
    GridView mGridView;

    @BindView(R.id.emptyTextView)
    TextView mEmptyView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.searchEditText)
    EditText mSearchEditText;

    @BindView(R.id.cancelSearchButton)
    ImageButton mCancelButton;

    @NonNull
    private MainActivityPresenter presenter;

    private final int ITEMS_FROM_END = 3;
    private ArrayList<Photo> photosArrayList;
    private PhotosListAdapter photosListAdapter;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        prepareAllViewsAndListeners();
        presenter = new MainActivityPresenter(this, this);
    }

    @Override
    public void goToInventory() {

    }

    private void prepareAllViewsAndListeners() {
        photosArrayList = new ArrayList<>();
        photosListAdapter = new PhotosListAdapter(this, photosArrayList);
        mGridView.setAdapter(photosListAdapter);

        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if ((listView.getLastVisiblePosition() >= listView.getCount() - ITEMS_FROM_END)) {
                        //load more list items:
                        presenter.getMoreFromServer();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                DialogImage aboutDialog = new DialogImage(MainActivity.this, photosArrayList.get(position), MainActivity.this);
                aboutDialog.openDialog();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.refreshItems();

            }
        });

        mSearchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    presenter.performSearch(mSearchEditText.getText().toString());
                    return true;
                }
                return false;
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchEditText.setText("");
                presenter.cancelSearch();
            }
        });
    }

    @Override
    public void updatePhotosList(ArrayList<Photo> posts) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        photosArrayList.addAll(posts);
        photosListAdapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String errorText) {
        if (!TextUtils.isEmpty(errorText)) {
            mEmptyView.setText(errorText);
        }
        runOnUiThread(new Runnable() {
            public void run() {
                mEmptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void clearList() {
        photosArrayList.clear();
    }

    @Override
    public void onDialogImageShareClick(Photo selectedPhoto) {
        if (dialog != null) dialog.show();
        Target image = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            File file = File.createTempFile("temp.jpeg", null, getCacheDir());
                            file.createNewFile();
                            FileOutputStream outstream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, outstream);
                            outstream.close();
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            Uri imageUri = Uri.parse(file.getPath());
                            sharingIntent.setType("image/png");
                            sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                            startActivity(sharingIntent);
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(this).load(selectedPhoto.getUrls().getRegular()).into(image);
    }

    @Override
    public void onDialogImageDownloadClick(Photo selectedPhoto) {
        if (dialog != null) dialog.show();
        Target image = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            File fold = new File(Environment.getExternalStorageDirectory().getPath()
                                    + "/" + BuildConfig.FILES_FOLDER);
                            if (!fold.exists()) {
                                fold.mkdirs();
                            }
                            File file = new File(fold, System.currentTimeMillis() + ".jpeg");
                            file.createNewFile();
                            FileOutputStream outstream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, outstream);
                            outstream.close();
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            final String path = file.getPath();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(MainActivity.this,
                                            getResources().getString(R.string.text_file_downloaded) + path, Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.text_file_download_error), Toast.LENGTH_SHORT).show();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(this).load(selectedPhoto.getUrls().getRegular()).into(image);
    }
}
