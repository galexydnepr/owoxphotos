package com.galexy.owoxphotos.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.galexy.owoxphotos.R;
import com.galexy.owoxphotos.models.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotosListAdapter extends BaseAdapter {

    private ArrayList<Photo> mPhotoArrayList;
    private LayoutInflater mLayoutInflater;
private Activity mActivity;
    public PhotosListAdapter(Activity activity, ArrayList<Photo> listData) {
        mActivity = activity;
        this.mPhotoArrayList = listData;
        mLayoutInflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return mPhotoArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPhotoArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_photo, null);
            holder = new ViewHolder();
            holder.mImageView = (ImageView) convertView.findViewById(R.id.photoPreview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mPhotoArrayList != null && mPhotoArrayList.size() > 0) {
            Photo photo = mPhotoArrayList.get(position);
            String path = mActivity.getResources().getBoolean(R.bool.isTablet) ? photo.getUrls().getRegular() :photo.getUrls().getSmall();
            Picasso.with(parent.getContext())
                    .load(path)
                    .into(holder.mImageView);
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView mImageView;
    }
}
